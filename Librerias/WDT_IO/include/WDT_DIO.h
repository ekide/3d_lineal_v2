//***************************************************************************
//  Copyright (c) 2013, Neousys Technology Inc. All rights reserved.
//
//  File Name:   WDT_DIO.h
//  Purpose:     Nuvo-1000/2000/2000+/1300af/3000/3304af & POC-100 Series
//               Watch-dog timer and isolated DIO Dynamic Linking Library
//  Revision:    2.0.5
//  Date:        May 21th, 2014
//
//  Note:        This libray uses WinIO v3.0 Library
//               WinIO v3.0 is the Copyright of Yariv Kaplan 1998-2010
//***************************************************************************/

#ifndef Neousys_WDT_DIO_Library
#define Neousys_WDT_DIO_Library

#pragma once

#ifndef BYTE
#define BYTE unsigned char
#endif

#ifndef WORD
#define WORD unsigned short
#endif

#ifndef DWORD
#define DWORD unsigned long
#endif

#ifndef BOOL
#define BOOL int
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(push, 1)

//Data structures for supporting DI Change-of-State interrupt (for Nuvo-3000 series and Nuvo-3304af)
typedef struct COS_INT_SETUP_T
{
    WORD    portMask;       // [in ] interrupt mask for corresponding DI channel(s), 0: disable 1: enable COS interrupt
    WORD    edgeMode;       // [in ] specify the condition of generating COS interrupt for corresponding DI channel(s), 0: level change 1: rising/falling edge
    WORD    edgeType;       // [in ] specify the rising/falling edge for corresponding DI channel(s), 0: rising edge 1: falling edge
}
COS_INT_SETUP, *COS_INT_SETUP_P;

typedef struct COS_INT_CALLBACK_ARG_T
{
    WORD    portData;       // [out] port input data
    WORD    intrFlag;       // [out] interrupt flag
    BYTE    intrSeq;        // [out] interrupt sequence number (0~255)
}
COS_INT_CALLBACK_ARG, *COS_INT_CALLBACK_ARG_P;

typedef void (__stdcall* COS_INT_CALLBACK)(COS_INT_CALLBACK_ARG* arg);

//Data structures for supporting Deterministic Trigger I/O (for Nuvo-3000 series and Nuvo-3304af)
//If trigSrcDI is configured for multiple pulseTgtDO(s), the last rising/falling edge setting is effecitve
typedef struct DTIO_SETUP_T
{
    BYTE    trigMode;       // [in ] 0: never triggered (disabled mode), 1: always triggered, 2: rising edge, 3: falling edge
    BYTE    trigSrcDI;      // [in ] single DI channel used as trigger source input (ex: 0~7 for Nuvo-3000)
    BYTE    pulseTgtDO;     // [in ] single DO channel used as pulse target output (ex: 0~7 for Nuvo-3000)
    BYTE    pulseExtra;     // [in ] some extra-parameter combination for DTIO function (ex: DTIO_INIT_HIGN)
    DWORD   pulseDelay;     // [in ] output pulse delay tick count, range: 2~2147483647
    DWORD   pulseWidth;     // [in ] output pulse width tick count, range: 1~2147483647
}
DTIO_SETUP, *DTIO_SETUP_P;

typedef struct DTFO_SETUP_T
{
    BYTE    trigMode;       // [in ] 0: never triggered (disabled mode), 1: reserved, 2: rising edge, 3: falling edge
    BYTE    trigSrcDI;      // [in ] single DI channel used as trigger source input (ex: 0~7 for Nuvo-3000)
    BYTE    pulseTgtDO;     // [in ] single DO channel used as pulse target output (ex: 0~7 for Nuvo-3000)
    BYTE    pulseExtra;     // [in ] some extra-parameter combination for DTFO function (ex: DTFO_INIT_HIGN)
    DWORD   pulseTag;       // [in ] reserved field
    DWORD   pulseWidth;     // [in ] output pulse width tick count, range: 1~2147483647
}
DTFO_SETUP, *DTFO_SETUP_P;

#define  DTIO_INIT_HIGN     (0x01)
#define  DTFO_INIT_HIGN     (0x01)

#pragma pack(pop)

//Register WinIO driver for non-administrator operation
BOOL RegisterWinIOService(void);    //Obselete function

//Isolated Digital I/O function
BOOL InitDIO(void);
BOOL DIReadLine(BYTE ch);
WORD DIReadPort(void);
void DOWriteLine(BYTE ch, BOOL value);
void DOWritePort(WORD value);
void DOWriteLineChecked(BYTE ch, BOOL value);
void DOWritePortChecked(WORD value);

//Watch-dog timer function
BOOL InitWDT(void);
BOOL StartWDT(void);
BOOL StopWDT(void);
BOOL SetWDT(WORD tick, BYTE unit);
BOOL ResetWDT(void);

//PoE ports enable/disable & deterministic trigger I/O (for Nuvo-3304af only)
BYTE GetStatusPoEPort(BYTE port);   //Get status of PoE port, 0: disabled 1: enabled
BOOL EnablePoEPort(BYTE port);      //Enable PoE port
BOOL DisablePoEPort(BYTE port);     //Disable PoE port

//DI Change-of-State notification
BOOL SetupDICOS(COS_INT_SETUP *lpSetup, DWORD cbSetup); //Setup DI Change-of-State interrupt parameters
BOOL StartDICOS(void);                                  //Start DI Change-of-State interrupt
BOOL StopDICOS(void);                                   //Stop DI Change-of-State interrupt
BOOL RegisterCallbackDICOS(COS_INT_CALLBACK callback);  //Register DI Change-of-State interrupt callback function

//Deterministic Trigger I/O
BOOL SetupDTIO(DTIO_SETUP *lpSetup, DWORD cbSetup);
BOOL StartDTIO(void);                       //[NOTE] If DICOS is started, this will stop it by internal invocation of StopDICOS
BOOL StopDTIO(void);
BOOL SetUnitDTIO(WORD unit, int delta);     //unit: DTIO timing unit in micro-second (25~2500 us), delta: fine-tuning timing unit, each +/- is 0.04us
WORD GetUnitDTIO(void);                     //Return current DTIO timing unit in micro-second

//Deterministic Trigger Fan Out
BOOL SetupDTFO(DTFO_SETUP *lpSetup, DWORD cbSetup);
BOOL StartDTFO(void);                       //[NOTE] If DICOS is started, this will stop it by internal invocation of StopDICOS
BOOL StopDTFO(void);
BOOL SetUnitDTFO(WORD unit, int delta);     //unit: DTFO timing unit in micro-second (25~2500 us), delta: fine-tuning timing unit, each +/- is 0.04us
WORD GetUnitDTFO(void);                     //Return current DTFO timing unit in micro-second

#ifdef __cplusplus
}
#endif

#endif
